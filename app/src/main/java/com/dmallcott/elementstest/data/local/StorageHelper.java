package com.dmallcott.elementstest.data.local;

import android.content.Context;

import com.cesarferreira.rxpaper.RxPaper;
import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.data.model.Item;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Helper class that handles communications with RxPaper.
 *
 * Created by dmallcott on 26/01/2016.
 */
public class StorageHelper {

    public static void saveItems(Context context, List<Item> items) {
        // First the previous values are cleared
        RxPaper.with(context, DataManager.BOOK_ITEMS)
                .destroy()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(); // I'm not checking on the result until I add retrolambda

        // Then the new ones are saved
        for (int i = 0; i < items.size(); i++)
            RxPaper.with(context, DataManager.BOOK_ITEMS)
                    .write(String.valueOf(i), items.get(i))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(); // I'm not checking on the result until I add retrolambda
    }
}
