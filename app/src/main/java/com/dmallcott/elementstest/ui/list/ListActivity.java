package com.dmallcott.elementstest.ui.list;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.dmallcott.elementstest.R;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * View class for the List package.
 *
 * Created by dmallcott on 25/01/2016.
 */
public class ListActivity extends BaseActivity implements ListContract.ListView {

    @Bind(R.id.toolbar) Toolbar tbMain;
    @Bind(R.id.recycler_view) RecyclerView rvList;

    @Inject ListPresenter mPresenter;
    ListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);

        setSupportActionBar(tbMain);

        mAdapter = new ListAdapter(this);

        // Setting up RecyclerView
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setItemAnimator(new SlideInUpAnimator(new AccelerateDecelerateInterpolator()));
        rvList.getItemAnimator().setAddDuration(500);
        rvList.setAdapter(mAdapter);

        mPresenter.loadData(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void showData(List<Item> items) {
        mAdapter.clear();

        for (Item item : items)
            mAdapter.add(item);
    }

    @Override
    public void showError(String message) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.activity_detail),
                message,
                Snackbar.LENGTH_SHORT
        );

        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.alert_red));
        snackbar.show();
    }
}
