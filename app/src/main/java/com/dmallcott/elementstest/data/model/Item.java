package com.dmallcott.elementstest.data.model;

import java.io.Serializable;

/**
 * Model class for csv item.
 *
 * Created by dmallcott on 25/01/2016.
 */
public class Item implements Serializable {
    public String title;
    public String description;
    public String image;

    public Item() {
    }

    public Item(String title, String description, String image) {
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public boolean isEmpty() {
        return title == null && description == null && image == null;
    }
}
