package com.dmallcott.elementstest;

import android.app.Application;
import android.content.Context;

import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.injection.component.ApplicationComponent;
import com.dmallcott.elementstest.injection.component.DaggerApplicationComponent;
import com.dmallcott.elementstest.injection.module.ApplicationModule;
import com.orm.SugarApp;

import javax.inject.Inject;

/**
 * Created by dmallcott on 25/01/2016.
 */
public class ElementsApplication extends Application {

    @Inject DataManager mDataManager;
    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);
    }

    public static ElementsApplication get(Context context) {
        return (ElementsApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
