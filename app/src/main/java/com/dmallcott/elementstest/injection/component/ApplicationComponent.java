package com.dmallcott.elementstest.injection.component;

import android.app.Application;
import android.content.Context;

import com.dmallcott.elementstest.ElementsApplication;
import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.injection.ApplicationContext;
import com.dmallcott.elementstest.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(ElementsApplication application);
    // Any service or injector goes here

    @ApplicationContext
    Context context();
    Application application();
    DataManager dataManager();
}
