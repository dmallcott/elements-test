package com.dmallcott.elementstest.common.injection.component;

import android.app.Application;
import android.content.Context;

import com.dmallcott.elementstest.common.injection.module.ApplicationTestModule;
import com.dmallcott.elementstest.injection.component.ApplicationComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
