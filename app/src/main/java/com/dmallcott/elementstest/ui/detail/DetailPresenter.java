package com.dmallcott.elementstest.ui.detail;

import android.content.Intent;

import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.list.ListContract;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Presenter class for the Detail package.
 *
 * Created by dmallcott on 26/01/2016.
 */
public class DetailPresenter implements DetailContract.DetailPresenter {

    DataManager mDataManager;
    DetailContract.DetailView mView;

    Item mItem;

    @Inject
    public DetailPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override
    public void attachView(DetailContract.DetailView mvpView) {
        mView = mvpView;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Override
    public void loadData(Intent intent) {
        mItem = (Item) intent.getSerializableExtra(DetailActivity.EXTRA_ITEM);

        if (mItem != null)
            mView.showData(mItem);
    }
}