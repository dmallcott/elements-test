package com.dmallcott.elementstest.ui.list;

import android.content.Context;
import android.util.Log;

import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.util.CSVReader;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.inject.Inject;

import retrofit.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Presenter class for the List package.
 *
 * Created by dmallcott on 25/01/2016.
 */
public class ListPresenter implements ListContract.ListPresenter {

    DataManager mDataManager;
    ListContract.ListView mView;
    public Subscription mSubscription;

    @Inject
    public ListPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override
    public void attachView(ListContract.ListView mvpView) {
        mView = mvpView;
        if (mSubscription != null)
            mSubscription.unsubscribe();
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Override
    public void loadData(Context context) {
        if (mSubscription != null) mSubscription.unsubscribe();

        Observable<List<Item>> observable = mDataManager.getItems(context)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

        mSubscription = observable.subscribe(new Subscriber<List<Item>>() {
            @Override
            public void onCompleted() {
                Log.d(ListPresenter.class.getSimpleName(), "Items completed");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(ListPresenter.class.getSimpleName() + " Error", e.getMessage());
                mView.showError(e.getMessage());
            }

            @Override
            public void onNext(List<Item> items) {
                Log.d(ListActivity.class.getSimpleName(), "Presenting " + items.size() + " items");
                mView.showData(items);
            }
        });
    }
}
