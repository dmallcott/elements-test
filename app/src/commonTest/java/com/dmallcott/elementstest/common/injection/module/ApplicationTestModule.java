package com.dmallcott.elementstest.common.injection.module;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;


import com.dmallcott.elementstest.data.DataManager;
import com.dmallcott.elementstest.data.remote.DataSheetService;
import com.dmallcott.elementstest.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

/**
 * Provides application-level dependencies for an app running on a testing environment
 * This allows injecting mocks if necessary.
 */
@Module
public class ApplicationTestModule {
    protected final Application mApplication;

    public ApplicationTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    /************* MOCKS *************/

    @Provides
    @Singleton
    DataManager providesDataManager() {
        return mock(DataManager.class);
    }

    @Provides
    @Singleton
    DataSheetService provideDataSheetService() {
        // Note: Normally I would mock this but I'm testing to see if it works

        return DataSheetService.Factory.makeDataSheetService();
        //return mock(DataSheetService.class);
    }
}
