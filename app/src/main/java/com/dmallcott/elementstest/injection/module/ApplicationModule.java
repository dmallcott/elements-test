package com.dmallcott.elementstest.injection.module;

import android.app.Application;
import android.content.Context;

import com.dmallcott.elementstest.data.remote.DataSheetService;
import com.dmallcott.elementstest.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies. Mainly singleton object that can be injected from
 * anywhere in the app.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    DataSheetService provideDataSheetService() {
        return DataSheetService.Factory.makeDataSheetService();
    }
}

