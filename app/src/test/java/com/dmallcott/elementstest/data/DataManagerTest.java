package com.dmallcott.elementstest.data;

import android.util.Log;

import com.dmallcott.elementstest.common.MockModelFabric;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.data.remote.DataSheetService;
import com.dmallcott.elementstest.util.CSVReader;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * This test class performs local unit tests without dependencies on the Android framework
 */
@RunWith(MockitoJUnitRunner.class)
public class DataManagerTest {

    @Mock DataSheetService mDataSheetService;
    DataManager mDataManager;

    @Before
    public void setUp() {
        mDataManager = Mockito.spy(new DataManager(mDataSheetService));
    }

    @Test
    public void testGetItems() throws Exception {
        // Removed test due to refactoring
    }
}