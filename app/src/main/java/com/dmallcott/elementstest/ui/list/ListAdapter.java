package com.dmallcott.elementstest.ui.list;

import android.content.Context;
import android.media.tv.TvContentRating;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dmallcott.elementstest.R;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.detail.DetailActivity;
import com.dmallcott.elementstest.util.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<Item> mDataSet;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public void add(Item item) {
        mDataSet.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        mDataSet.clear();
        notifyDataSetChanged();
    }

    public ListAdapter(Context context) {
        mContext = context;
        mDataSet = new ArrayList<>();
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder((mLayoutInflater.inflate(R.layout.row_item, parent, false)));
    }

    @Override
    public void onBindViewHolder(ListAdapter.ViewHolder holder, int position) {
        final Item item = mDataSet.get(position);
        holder.tvTitle.setText(item.title);
        holder.tvDescription.setText(item.description);
        holder.rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(DetailActivity.getIntent(mContext, item));
            }
        });

        try {
            Picasso.with(mContext)
                    .load(item.image)
                    .fit()
                    .placeholder(R.mipmap.ic_picture_placeholder)
                    .error(R.drawable.ic_error_image)
                    .transform(new CircleTransformation())
                    .into(holder.ivImage);
        } catch (IllegalArgumentException e) {
            // Do nothing since builder already does
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_title)
        TextView tvTitle;

        @Bind(R.id.item_description)
        TextView tvDescription;

        @Bind(R.id.item_image)
        ImageView ivImage;

        @Bind(R.id.root_layout)
        RelativeLayout rlRoot;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
