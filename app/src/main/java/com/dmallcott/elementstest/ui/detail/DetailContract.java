package com.dmallcott.elementstest.ui.detail;

import android.content.Context;
import android.content.Intent;

import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.base.MvpView;
import com.dmallcott.elementstest.ui.base.Presenter;

/**
 * Contract interface for implementing MVP pattern between {@link DetailActivity} and
 * {@link com.dmallcott.elementstest.ui.list.DetailPresenter}
 *
 * Created by dmallcott on 26/01/2016.
 */
public interface DetailContract {
    interface DetailView extends MvpView {
        void showData(Item item);

        void showError(String message);
    }

    interface DetailPresenter extends Presenter<DetailView> {
        void loadData(Intent intent);
    }
}
