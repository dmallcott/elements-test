package com.dmallcott.elementstest.injection.component;

import com.dmallcott.elementstest.injection.PerActivity;
import com.dmallcott.elementstest.injection.module.ActivityModule;
import com.dmallcott.elementstest.ui.detail.DetailActivity;
import com.dmallcott.elementstest.ui.list.ListActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ListActivity activity);
    void inject(DetailActivity activity);
}

