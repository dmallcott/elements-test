package com.dmallcott.elementstest.data.remote;

import com.dmallcott.elementstest.BuildConfig;
import com.dmallcott.elementstest.data.model.Item;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.Field;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.Streaming;
import rx.Observable;

/**
 * Retrofit interface for data sheets API.
 *
 * Created by dmallcott on 25/01/2016.
 */
public interface DataSheetService {

    String ENDPOINT = "https://docs.google.com/spreadsheet/";

    @Streaming
    @GET("ccc?single=true&gid=0&output=csv")
    Observable<ResponseBody> getCSV(@Query("key") String key);

    /******** Factory class that sets up a new service *******/
    class Factory {
        public static DataSheetService makeDataSheetService() {
            OkHttpClient okHttpClient = new OkHttpClient();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);
            okHttpClient.interceptors().add(logging);

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(DataSheetService.ENDPOINT)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(DataSheetService.class);
        }
    }
}
