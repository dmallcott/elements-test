package com.dmallcott.elementstest.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmallcott.elementstest.R;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.base.BaseActivity;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity implements DetailContract.DetailView {

    public static final String EXTRA_ITEM = "extra_item";

    @Bind(R.id.toolbar) Toolbar tbMain;
    @Bind(R.id.item_image) ImageView ivImage;
    @Bind(R.id.item_description) TextView tvDescription;

    @Inject DetailPresenter mPresenter;

    public static Intent getIntent(Context context, Item item) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_ITEM, item);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);

        setSupportActionBar(tbMain);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.loadData(getIntent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void showData(Item item) {
        getSupportActionBar().setTitle(item.title);
        tvDescription.setText(item.description);

        try {
            Picasso.with(this)
                    .load(item.image)
                    .fit()
                    .centerCrop()
                    .into(ivImage);
        } catch (IllegalArgumentException e) {
            // Do nothing since builder already does
        }
    }

    @Override
    public void showError(String message) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.list_activity),
                message,
                Snackbar.LENGTH_SHORT
        );

        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.alert_red));
        snackbar.show();
    }
}
