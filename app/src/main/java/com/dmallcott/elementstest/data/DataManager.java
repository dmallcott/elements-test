package com.dmallcott.elementstest.data;

import android.content.Context;
import android.util.Log;

import com.cesarferreira.rxpaper.RxPaper;
import com.dmallcott.elementstest.BuildConfig;
import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.data.remote.DataSheetService;
import com.dmallcott.elementstest.ui.list.ListActivity;
import com.dmallcott.elementstest.util.CSVReader;
import com.squareup.okhttp.ResponseBody;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.dmallcott.elementstest.data.local.StorageHelper.saveItems;

/**
 * Will handle the presenter's requests following this architectural pattern:
 * <p/>
 * https://github.com/ribot/android-guidelines/blob/master/architecture_guidelines/android_architecture.md
 * <p/>
 * Created by dmallcott on 21/01/2016.
 */
@Singleton
public class DataManager {

    public static final String BOOK_ITEMS = "book_items";
    private final DataSheetService mDataSheetService;

    @Inject
    public DataManager(DataSheetService dataSheetService) {
        mDataSheetService = dataSheetService;
    }

    public Observable<List<Item>> getItems(Context context) {
        Observable<List<Item>> localItems = getLocalItems(context);
        Observable<List<Item>> remoteItems = getRemoteItems(context);

        return Observable.merge(localItems, remoteItems);
    }

    private Observable<List<Item>> getLocalItems(final Context context) {
        return RxPaper.with(context, BOOK_ITEMS).getAllKeys().map(new Func1<List<String>, List<Item>>() {
            @Override
            public List<Item> call(List<String> keys) {
                final List<Item> items = new ArrayList<Item>();

                for (String key : keys)
                    RxPaper.with(context, BOOK_ITEMS)
                            .read(key, new Item())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<Item>() {
                                @Override
                                public void onCompleted() {
                                    Log.d(DataManager.class.getSimpleName(), "Local items " +
                                            "completed");
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.e(DataManager.class.getSimpleName(), "Local items error");
                                }

                                @Override
                                public void onNext(Item item) {
                                    if (!item.isEmpty()) {
                                        items.add(item);
                                    }
                                }
                            });

                Log.d(DataManager.class.getSimpleName(), "Returning " + items.size() + " local " +
                        "items");
                return items;
            }
        });
    }

    private Observable<List<Item>> getRemoteItems(final Context context) {
        return mDataSheetService.getCSV(BuildConfig.SHEET_KEY)
                .map(new Func1<ResponseBody, List<Item>>() {
                    @Override
                    public List<Item> call(ResponseBody response) {
                        List<Item> items = new ArrayList<Item>();
                        String next[] = {};
                        InputStreamReader inputStreamReader;

                        try {
                            inputStreamReader = new InputStreamReader(response.byteStream());
                            CSVReader reader = new CSVReader(inputStreamReader);
                            reader.readNext(); // The first line is skipped
                            next = reader.readNext();

                            while (next != null) {
                                items.add(new Item(next[0], next[1], next[2]));
                                next = reader.readNext();
                            }

                            reader.close();
                        } catch (Exception e) {
                            Log.e(DataManager.class.getSimpleName(), "Remote items error");
                            // Do nothing for now
                            return new ArrayList<Item>();
                        }

                        // If there were no problems the newly fetched items are saved
                        saveItems(context, items);

                        Log.d(DataManager.class.getSimpleName(), "Returning " + items.size() +
                                " remote items");
                        return items;
                    }
                });
    }
}
