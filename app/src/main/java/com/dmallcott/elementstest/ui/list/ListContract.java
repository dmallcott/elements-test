package com.dmallcott.elementstest.ui.list;

import android.content.Context;

import com.dmallcott.elementstest.data.model.Item;
import com.dmallcott.elementstest.ui.base.MvpView;
import com.dmallcott.elementstest.ui.base.Presenter;

import java.util.List;

/**
 * Contract interface for implementing MVP pattern between {@link ListActivity} and
 * {@link com.dmallcott.elementstest.ui.list.ListPresenter}
 *
 * Created by dmallcott on 25/01/2016.
 */
public interface ListContract {
    interface ListView extends MvpView {
        void showData(List<Item> items);

        void showError(String message);
    }

    interface ListPresenter extends Presenter<ListView> {
        void loadData(Context context);
    }
}
